import * as $ from 'jquery'
import * as monaco from 'monaco-editor'

window.monaco_editors = []

$('.monaco_editor_widget').each(function () {
  const widget = $(this)
  const textarea = widget.find('textarea').first()
  const container = widget.find('.monaco_editor_container').first()

  const language = textarea.attr('data-language')

  const editor = monaco.editor.create(container[0], {
    value: textarea.val(),
    language: language
  })

  editor.onDidBlurEditorText(function (e) {
    textarea.val(editor.getValue())
  })

  window.monaco_editors.push(editor)
})

let width = $(window).width()

$(window).on('resize', function () {
  if ($(this).width() !== width) {
    width = $(this).width()

    window.monaco_editors.map(function (editor) {
      editor.layout()
    })
  }
})
