import django_filters
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Field, Submit

from apps.business.exercise.models import ExerciseSubmissionLog


class SubmissionLogFilter(django_filters.FilterSet):
    #  https://github.com/carltongibson/django-filter/issues/522#issuecomment-258171253
    tests_passed = django_filters.ChoiceFilter(choices=((True, "Yes"), (False, "No")))

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.form.helper = FormHelper()
        self.form.helper.layout = self.get_layout()
        self.form.helper.disable_csrf = True
        self.form.helper.form_method = 'get'

    @staticmethod
    def get_layout():
        return Layout(
            Field('author'),
            Field('author__groups'),
            Field('tests_passed'),
            Field('is_solved'),

            Submit('filter', 'Filter')
        )

    class Meta:
        model = ExerciseSubmissionLog
        fields = ['author', 'author__groups']  # https://github.com/carltongibson/django-filter/issues/1012
