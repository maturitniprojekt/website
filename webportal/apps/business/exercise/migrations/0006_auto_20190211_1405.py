# Generated by Django 2.1.4 on 2019-02-11 14:05

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('exercise', '0005_exercisesubmissionlog_is_final'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='exercisesubmissionlog',
            options={'default_permissions': (), 'ordering': ['-created_at']},
        ),
        migrations.AlterField(
            model_name='exercisesubmissionlog',
            name='author',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterField(
            model_name='exercisesubmissionlog',
            name='exercise',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='exercise.Exercise'),
        ),
    ]
