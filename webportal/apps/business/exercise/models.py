from django.contrib.contenttypes.fields import GenericRelation
from django.contrib.contenttypes.models import ContentType
from django.db import models
from django.urls import reverse

from apps.business.section.models import SectionItem
from apps.core.account.models import User


class Exercise(models.Model):
    title = models.CharField(max_length=255)
    description = models.TextField()

    code_skeleton = models.TextField()
    public_tests = models.TextField()
    secret_tests = models.TextField()

    objects = models.Manager()

    # For CASCADE functionality for generic foreign key
    section_items = GenericRelation(SectionItem, related_query_name='exercise')

    @property
    def section(self):
        return self.section_items.first().section

    def get_absolute_url(self):
        return reverse('exercise:detail', kwargs={'pk': self.pk})

    def get_update_url(self):
        return reverse('exercise:update', kwargs={'pk': self.pk})

    def get_delete_url(self):
        return reverse('exercise:delete', kwargs={'pk': self.pk})

    def __str__(self):
        return self.title

    class Meta:
        default_permissions = ()


class ExerciseSubmissionLog(models.Model):
    exercise = models.ForeignKey(Exercise, on_delete=models.CASCADE)
    author = models.ForeignKey(User, on_delete=models.CASCADE)

    code = models.TextField()
    tests = models.TextField()

    tests_passed = models.BooleanField(default=False)
    is_final = models.BooleanField(default=False)

    created_at = models.DateTimeField(auto_now_add=True)

    objects = models.Manager()

    class Meta:
        ordering = ['-created_at']
        default_permissions = ()
