from django.urls import path

from . import views

app_name = 'exercise'
urlpatterns = [
    path('create/', views.ExerciseCreateView.as_view(), name='create'),
    path('<int:pk>/update/', views.ExerciseUpdateView.as_view(), name='update'),
    path('<int:pk>/detail/', views.ExerciseDetailView.as_view(), name='detail'),
    path('<int:pk>/delete/', views.ExerciseDeleteView.as_view(), name='delete'),
    path('<int:pk>/submission_logs/', views.ExerciseSubmissionLogListView.as_view(), name='submission_logs'),
]
