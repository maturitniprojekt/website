from webportal.admin import clientadmin
from .models import Exercise, ExerciseSubmissionLog

clientadmin.register(Exercise)
clientadmin.register(ExerciseSubmissionLog)
