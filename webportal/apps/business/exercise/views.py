import os

import requests
from django.http import JsonResponse
from django.shortcuts import get_object_or_404
from django.views.generic import CreateView, UpdateView, FormView, DeleteView, ListView
from guardian.mixins import PermissionRequiredMixin

from apps.business.section.models import Section, SectionItem
from apps.core.account.mixins import AnyPermissionRequiredMixin
from . import forms, filters
from .models import Exercise, ExerciseSubmissionLog


class ExerciseCreateView(PermissionRequiredMixin, CreateView):
    permission_required = 'course.manage_course'

    model = Exercise
    form_class = forms.ExerciseCreateForm
    template_name = 'exercise/create.html'

    def dispatch(self, request, *args, **kwargs):
        self.section = get_object_or_404(Section, pk=self.request.GET.get('section_pk'))
        return super().dispatch(request, *args, **kwargs)

    def get_permission_object(self):
        return self.section.course

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs.update({
            'lang': self.section.course.programming_language.editor_name
        })
        return kwargs

    def get_success_url(self):
        return self.section.course.get_absolute_url()

    def form_valid(self, form):
        exercise = form.save()
        SectionItem.objects.create(section=self.section, content_object=exercise)
        return super().form_valid(form)


class ExerciseUpdateView(PermissionRequiredMixin, UpdateView):
    permission_required = 'course.manage_course'

    model = Exercise
    form_class = forms.ExerciseUpdateForm
    template_name = 'exercise/update.html'

    def get_permission_object(self):
        return self.get_object().section.course

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs.update({
            'lang': self.object.section.course.programming_language.editor_name
        })
        return kwargs

    def get_success_url(self):
        return self.object.section.course.get_absolute_url()


class ExerciseDeleteView(PermissionRequiredMixin, DeleteView):
    permission_required = 'course.manage_course'

    model = Exercise
    template_name = 'exercise/delete.html'

    def get_permission_object(self):
        return self.get_object().section.course

    def get_success_url(self):
        return self.object.section.course.get_absolute_url()


class ExerciseDetailView(AnyPermissionRequiredMixin, FormView):
    permission_required = ['course.manage_course', 'course.view_course']

    model = Exercise
    form_class = forms.ExerciseSubmissionForm
    template_name = 'exercise/detail.html'

    def dispatch(self, request, *args, **kwargs):
        self.exercise = get_object_or_404(Exercise, pk=kwargs.get('pk'))
        self.course = self.exercise.section.course
        self.programming_language = self.exercise.section.course.programming_language
        return super().dispatch(request, *args, **kwargs)

    def get_permission_object(self):
        return self.course

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs.update({
            'initial': {
                'code': self.exercise.code_skeleton,
                'tests': self.exercise.public_tests
            },
            'lang': self.programming_language.editor_name
        })
        return kwargs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'exercise': self.exercise,
            'course': self.course,
            'exercise_submission_logs': self.exercise.exercisesubmissionlog_set.filter(author=self.request.user)
        })
        return context

    def post(self, request, *args, **kwargs):
        form = self.get_form()

        if form.is_valid():
            is_final = form.cleaned_data['is_final']
            user_code = form.cleaned_data['code']
            user_tests = form.cleaned_data['tests']

            exercise_submission_log = ExerciseSubmissionLog(
                exercise=self.exercise,
                author=request.user,
                code=user_code,
                tests=user_tests,
                is_final=is_final
            )

            code_runner_params = {
                'lang': self.programming_language.code_runner_name,
                'code': user_code,
                'tests': self.exercise.secret_tests if is_final else user_tests,
            }

            try:
                code_runner_address = os.environ.get('CODE_RUNNER_ADDRESS')
                r = requests.post(f'{code_runner_address}/executor/execute-tests', data=code_runner_params)
                r_json = r.json()

                result = r_json['output']['result']['convertedResult']

                if result and result['failures'] == 0:
                    exercise_submission_log.tests_passed = True

                exercise_submission_log.save()

                response = {'data': r_json}
            except requests.exceptions.RequestException:
                response = {'data': {'error': 'CODE_RUNNER_CONNECTION_ERROR'}, 'status': 500}

            exercise_submission_log.save()

            return JsonResponse(**response)


class ExerciseSubmissionLogListView(PermissionRequiredMixin, ListView):
    permission_required = 'course.manage_course'

    model = ExerciseSubmissionLog
    context_object_name = 'submission_logs'
    template_name = 'exercise/submission_logs.html'

    def dispatch(self, request, *args, **kwargs):
        self.exercise = get_object_or_404(Exercise, pk=self.kwargs.get('pk'))
        return super().dispatch(request, *args, **kwargs)

    def get_permission_object(self):
        return self.exercise.section.course

    def get_queryset(self, *args, **kwargs):
        qs = super().get_queryset().filter(exercise=self.exercise)
        self.filter = filters.SubmissionLogFilter(self.request.GET, queryset=qs)
        return self.filter.qs

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data()
        context.update({
            'filter': self.filter
        })
        return context
