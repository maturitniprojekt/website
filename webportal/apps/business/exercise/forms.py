from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Field, Submit, Div
from django import forms

from apps.platform.monaco_editor.widgets import MonacoEditor
from .models import Exercise


class ExerciseCreateForm(forms.ModelForm):
    def __init__(self, lang, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.fields['code_skeleton'].widget = MonacoEditor(language=lang)
        self.fields['public_tests'].widget = MonacoEditor(language=lang)
        self.fields['secret_tests'].widget = MonacoEditor(language=lang)

        self.helper = FormHelper()
        self.helper.layout = self.get_layout()

    @staticmethod
    def get_layout():
        return Layout(
            Field('title'),
            Field('description'),
            Field('code_skeleton'),
            Field('public_tests'),
            Field('secret_tests'),

            Submit('create', 'Create exercise')
        )

    class Meta:
        model = Exercise
        fields = ['title', 'description', 'code_skeleton', 'public_tests', 'secret_tests']


class ExerciseUpdateForm(forms.ModelForm):
    def __init__(self, lang, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.fields['code_skeleton'].widget = MonacoEditor(language=lang)
        self.fields['public_tests'].widget = MonacoEditor(language=lang)
        self.fields['secret_tests'].widget = MonacoEditor(language=lang)

        self.helper = FormHelper()
        self.helper.layout = self.get_layout()

    @staticmethod
    def get_layout():
        return Layout(
            Field('title'),
            Field('description'),
            Field('code_skeleton'),
            Field('public_tests'),
            Field('secret_tests'),

            Submit('update', 'Update exercise')
        )

    class Meta:
        model = Exercise
        fields = ['title', 'description', 'code_skeleton', 'public_tests', 'secret_tests']


class ExerciseSubmissionForm(forms.Form):
    code = forms.CharField(required=False)
    tests = forms.CharField(required=False)
    is_final = forms.BooleanField(required=False)

    def __init__(self, lang, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.fields['code'].widget = MonacoEditor(language=lang)
        self.fields['tests'].widget = MonacoEditor(language=lang)

        self.helper = FormHelper()
        self.helper.layout = self.get_layout()

        self.helper.form_id = 'exerciseSubmissionForm'

    @staticmethod
    def get_layout():
        return Layout(
            Field('code'),
            Field('tests'),
            Div(
                Div(
                    Field('is_final'),
                    css_class='col-2'
                ),
                Div(
                    Submit('submit', 'Submit exercise'),
                    css_class='col'
                ),
                css_class='form-inline form-row'
            )

        )
