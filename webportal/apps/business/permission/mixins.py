from guardian.shortcuts import get_users_with_perms, get_groups_with_perms


class ModelObjectLevelPermissionsMixin:
    def get_users_with_perms(self, permission):
        anyperm = get_users_with_perms(self, attach_perms=True)
        result = []
        for user, perms in anyperm.items():
            if permission in perms:
                result.append(user)
        return result

    def get_groups_with_perms(self, permission):
        anyperm = get_groups_with_perms(self, attach_perms=True)
        result = []
        for user, perms in anyperm.items():
            if permission in perms:
                result.append(user)
        return result

    class Meta:
        abstract = True
