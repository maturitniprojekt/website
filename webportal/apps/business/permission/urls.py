from django.urls import include, path

from apps.business.permission import views

specific_course_urls = [
    path('manager_groups/', views.CourseManagerGroupsView.as_view(), name='manager_groups'),
    path('manager_users/', views.CourseManagerUsersView.as_view(), name='manager_users'),

    path('learner_groups/', views.CourseLearnerGroupsView.as_view(), name='learner_groups'),
    path('learner_users/', views.CourseLearnerUsersView.as_view(), name='learner_users'),
]

app_name = 'permission'
urlpatterns = [
    path('course/<int:course_pk>/', include((specific_course_urls, 'course'))),
]
