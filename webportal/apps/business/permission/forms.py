from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Field, Submit
from django import forms
from django.contrib.auth.models import Group

from apps.core.account.models import User


class CourseManagerGroupsForm(forms.Form):
    groups = forms.ModelMultipleChoiceField(queryset=Group.objects.all(), required=False)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.helper = FormHelper()
        self.helper.layout = self.get_layout()

    def get_layout(self):
        return Layout(
            Field('groups'),

            Submit('update', 'Update group managers')
        )


class CourseManagerUsersForm(forms.Form):
    users = forms.ModelMultipleChoiceField(queryset=User.objects.all(), required=False)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.helper = FormHelper()
        self.helper.layout = self.get_layout()

    def get_layout(self):
        return Layout(
            Field('users'),

            Submit('update', 'Update user managers')
        )


class CourseLearnerGroupsForm(forms.Form):
    groups = forms.ModelMultipleChoiceField(queryset=Group.objects.all(), required=False)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.helper = FormHelper()
        self.helper.layout = self.get_layout()

    def get_layout(self):
        return Layout(
            Field('groups'),

            Submit('update', 'Update group learners')
        )


class CourseLearnerUsersForm(forms.Form):
    users = forms.ModelMultipleChoiceField(queryset=User.objects.all(), required=False)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.helper = FormHelper()
        self.helper.layout = self.get_layout()

    def get_layout(self):
        return Layout(
            Field('users'),

            Submit('update', 'Update user learners')
        )
