from django.contrib.auth.models import Group
from django.shortcuts import get_object_or_404
from django.views.generic import FormView
from guardian.mixins import PermissionRequiredMixin
from guardian.shortcuts import remove_perm, assign_perm

from apps.business.course.models import Course
from apps.core.account.models import User
from . import forms


# ----------
# Manager permissions to course
# ----------
class CourseManagerGroupsView(PermissionRequiredMixin, FormView):
    permission_required = 'course.manage_course'

    form_class = forms.CourseManagerGroupsForm
    template_name = 'permission/course/manager_groups.html'

    def dispatch(self, request, *args, **kwargs):
        self.course = get_object_or_404(Course, pk=self.kwargs.get('course_pk'))
        return super().dispatch(request, *args, **kwargs)

    def get_permission_object(self):
        return self.course

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs.update({
            'initial': {
                'groups': self.course.get_groups_with_perms('manage_course')
            }
        })
        return kwargs

    def form_valid(self, form):
        all_groups = Group.objects.all()
        selected_groups = form.cleaned_data['groups']

        to_remove = set(all_groups) - set(selected_groups)

        for group in to_remove:
            remove_perm('manage_course', group, self.course)

        for group in selected_groups:
            assign_perm('manage_course', group, self.course)

        return super().form_valid(form)

    def get_success_url(self):
        return self.request.GET.get('next', self.course.get_absolute_url())


class CourseManagerUsersView(PermissionRequiredMixin, FormView):
    permission_required = 'course.manage_course'

    form_class = forms.CourseManagerUsersForm
    template_name = 'permission/course/manager_users.html'

    def dispatch(self, request, *args, **kwargs):
        self.course = get_object_or_404(Course, pk=self.kwargs.get('course_pk'))
        return super().dispatch(request, *args, **kwargs)

    def get_permission_object(self):
        return self.course

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs.update({
            'initial': {
                'users': self.course.get_users_with_perms('manage_course')
            }
        })
        return kwargs

    def form_valid(self, form):
        all_users = User.objects.all()
        selected_users = form.cleaned_data['users']

        to_remove = set(all_users) - set(selected_users)

        for user in to_remove:
            remove_perm('manage_course', user, self.course)

        for user in selected_users:
            assign_perm('manage_course', user, self.course)

        return super().form_valid(form)

    def get_success_url(self):
        return self.request.GET.get('next', self.course.get_absolute_url())


# ----------
# Learner permissions to course
# ----------
class CourseLearnerGroupsView(PermissionRequiredMixin, FormView):
    permission_required = 'course.manage_course'

    form_class = forms.CourseLearnerGroupsForm
    template_name = 'permission/course/learner_groups.html'

    def dispatch(self, request, *args, **kwargs):
        self.course = get_object_or_404(Course, pk=self.kwargs.get('course_pk'))
        return super().dispatch(request, *args, **kwargs)

    def get_permission_object(self):
        return self.course

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs.update({
            'initial': {
                'groups': self.course.get_groups_with_perms('view_course')
            }
        })
        return kwargs

    def form_valid(self, form):
        all_groups = Group.objects.all()
        selected_groups = form.cleaned_data['groups']

        to_remove = set(all_groups) - set(selected_groups)

        for group in to_remove:
            remove_perm('view_course', group, self.course)

        for group in selected_groups:
            assign_perm('view_course', group, self.course)

        return super().form_valid(form)

    def get_success_url(self):
        return self.request.GET.get('next', self.course.get_absolute_url())


class CourseLearnerUsersView(PermissionRequiredMixin, FormView):
    permission_required = 'course.manage_course'

    form_class = forms.CourseLearnerUsersForm
    template_name = 'permission/course/learner_users.html'

    def dispatch(self, request, *args, **kwargs):
        self.course = get_object_or_404(Course, pk=self.kwargs.get('course_pk'))
        return super().dispatch(request, *args, **kwargs)

    def get_permission_object(self):
        return self.course

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        print(self.course.get_users_with_perms('view_course'))
        kwargs.update({
            'initial': {
                'users': self.course.get_users_with_perms('view_course')
            }
        })
        return kwargs

    def form_valid(self, form):
        all_users = User.objects.all()
        selected_users = form.cleaned_data['users']

        to_remove = set(all_users) - set(selected_users)

        print(to_remove)
        print(all_users)
        print(selected_users)

        for user in to_remove:
            remove_perm('view_course', user, self.course)

        for user in selected_users:
            assign_perm('view_course', user, self.course)

        return super().form_valid(form)

    def get_success_url(self):
        return self.request.GET.get('next', self.course.get_absolute_url())

# class UsersPermissionsView(PermissionRequiredMixin, ListView):
#     permission_required = 'course.manage_course'
#
#     course = None
#
#     model = User
#     template_name = 'permission/user_list.html'
#
#     def dispatch(self, request, *args, **kwargs):
#         self.course = Course.objects.get(pk=kwargs.get('course_pk'))
#         return super().dispatch(request, *args, **kwargs)
#
#     def get_permission_object(self):
#         return self.course
#
#     def get_context_data(self, **kwargs):
#         context = super().get_context_data(**kwargs)
#         context.update({
#             'course': self.course,
#         })
#         return context
#
#
# class UserPermissionsView(PermissionRequiredMixin, FormView):
#     permission_required = 'course.manage_course'
#
#     course = None
#     user = None
#
#     form_class = forms.UserPermissionsForm
#     template_name = 'permission/user_detail.html'
#
#     def get_success_url(self):
#         return reverse('permission:course:users', kwargs={'course_pk': self.course.pk})
#
#     def dispatch(self, request, *args, **kwargs):
#         self.course = Course.objects.get(pk=kwargs.get('course_pk'))
#         self.user = User.objects.get(pk=self.kwargs.get('user_pk'))
#         return super().dispatch(request, *args, **kwargs)
#
#     def get_permission_object(self):
#         return self.course
#
#     def get_form_kwargs(self):
#         kwargs = super().get_form_kwargs()
#         kwargs.update({
#             'user': self.user,
#             'obj': self.course
#         })
#         return kwargs
#
#     def form_valid(self, form):
#         form.save_obj_perms()
#         return super().form_valid(form)
