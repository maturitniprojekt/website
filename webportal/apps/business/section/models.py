from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.db import models
from django.urls import reverse

from apps.business.course.models import Course


class Section(models.Model):
    course = models.ForeignKey(Course, on_delete=models.PROTECT)

    title = models.CharField(max_length=255)
    description = models.TextField(blank=True, null=True)

    sort_order = models.IntegerField(default=0)

    objects = models.Manager()

    def get_update_url(self):
        return reverse('section:update', kwargs={'pk': self.pk})

    def get_delete_url(self):
        return reverse('section:delete', kwargs={'pk': self.pk})

    def __str__(self):
        return self.title

    class Meta:
        ordering = ['-sort_order']
        default_permissions = ()


class SectionItem(models.Model):
    section = models.ForeignKey(Section, on_delete=models.PROTECT)

    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey('content_type', 'object_id')

    sort_order = models.IntegerField(default=0)

    objects = models.Manager()

    def __str__(self):
        return f'{self.section.course} | {self.section} | {self.content_object} ({self.content_type})'

    class Meta:
        unique_together = ('content_type', 'object_id')
        ordering = ['-object_id', '-sort_order']
        default_permissions = ()
