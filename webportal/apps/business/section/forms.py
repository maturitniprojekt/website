from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Field, Submit
from django import forms

from .models import Section


class SectionCreateForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.helper = FormHelper()
        self.helper.layout = self.get_layout()

    @staticmethod
    def get_layout():
        return Layout(
            Field('title'),
            Field('description'),

            Submit('create', 'Create section')
        )

    class Meta:
        model = Section
        fields = ['title', 'description']


class SectionUpdateForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.helper = FormHelper()
        self.helper.layout = self.get_layout()

    @staticmethod
    def get_layout():
        return Layout(
            Field('title'),
            Field('description'),

            Submit('update', 'Update section')
        )

    class Meta:
        model = Section
        fields = ['title', 'description']
