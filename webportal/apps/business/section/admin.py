from webportal.admin import clientadmin
from .models import Section, SectionItem

clientadmin.register(Section)
clientadmin.register(SectionItem)
