from django.shortcuts import get_object_or_404
from django.views.generic import CreateView, UpdateView, DeleteView
from guardian.mixins import PermissionRequiredMixin

from apps.business.course.models import Course
from . import forms, models


class SectionCreateView(PermissionRequiredMixin, CreateView):
    permission_required = 'course.manage_course'

    model = models.Section
    form_class = forms.SectionCreateForm
    template_name = 'section/create.html'

    def dispatch(self, request, *args, **kwargs):
        self.course = get_object_or_404(Course, pk=self.request.GET.get('course_pk'))
        return super().dispatch(request, *args, **kwargs)

    def get_permission_object(self):
        return self.course

    def get_success_url(self):
        return self.request.GET.get('next', self.course.get_absolute_url())

    def form_valid(self, form):
        section = form.save(commit=False)
        section.course = self.course
        section.save()
        return super().form_valid(form)


class SectionUpdateView(PermissionRequiredMixin, UpdateView):
    permission_required = 'course.manage_course'

    model = models.Section
    form_class = forms.SectionUpdateForm
    template_name = 'section/update.html'

    def get_permission_object(self):
        return self.get_object().course

    def get_success_url(self):
        return self.object.course.get_absolute_url()


class SectionDeleteView(PermissionRequiredMixin, DeleteView):
    permission_required = 'course.manage_course'

    model = models.Section
    template_name = 'section/delete.html'

    def get_permission_object(self):
        return self.get_object().course

    def get_success_url(self):
        return self.object.course.get_absolute_url()
