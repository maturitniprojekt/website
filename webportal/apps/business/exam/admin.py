from webportal.admin import clientadmin
from .models import Exam, ExamSubmissionLog

clientadmin.register(Exam)
clientadmin.register(ExamSubmissionLog)
