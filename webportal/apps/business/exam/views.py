from django.shortcuts import get_object_or_404
from django.views.generic import CreateView, DetailView, UpdateView

from apps.business.section.models import Section, SectionItem
from . import forms
from .models import Exam


# TODO add permissions
class ExamCreateView(CreateView):
    model = Exam
    form_class = forms.ExamCreateForm
    template_name = 'exam/create.html'

    def dispatch(self, request, *args, **kwargs):
        self.section = get_object_or_404(Section, pk=self.request.GET.get('section_pk'))
        return super().dispatch(request, *args, **kwargs)

    def get_success_url(self):
        return self.section.course.get_absolute_url()

    def form_valid(self, form):
        exam = form.save()
        SectionItem.objects.create(section=self.section, content_object=exam)
        return super().form_valid(form)


# TODO add permissions
class ExamUpdateView(UpdateView):
    model = Exam
    form_class = forms.ExamUpdateForm
    template_name = 'exam/update.html'

    def get_success_url(self):
        return self.object.section.course.get_absolute_url()


# TODO add permissions
class ExamDetailView(DetailView):
    model = Exam
    template_name = 'exam/detail.html'
