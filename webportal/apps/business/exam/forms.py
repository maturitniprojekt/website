from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Field, Submit
from django import forms

from .models import Exam


class ExamCreateForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.helper = FormHelper()
        self.helper.layout = self.get_layout()

    @staticmethod
    def get_layout():
        return Layout(
            Field('title'),
            Field('description'),
            Field('code_skeleton'),
            Field('public_tests'),
            Field('secret_tests'),

            Submit('create', 'Create exam')
        )

    class Meta:
        model = Exam
        fields = ['title', 'description', 'code_skeleton', 'public_tests', 'secret_tests']


class ExamUpdateForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.helper = FormHelper()
        self.helper.layout = self.get_layout()

    @staticmethod
    def get_layout():
        return Layout(
            Field('title'),
            Field('description'),
            Field('code_skeleton'),
            Field('public_tests'),
            Field('secret_tests'),

            Submit('update', 'Update exam')
        )

    class Meta:
        model = Exam
        fields = ['title', 'description', 'code_skeleton', 'public_tests', 'secret_tests']
