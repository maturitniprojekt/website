# Generated by Django 2.1.4 on 2019-02-09 06:58

import django.db.models.deletion
from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):
    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Exam',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=255)),
                ('description', models.TextField()),
                ('code_skeleton', models.TextField()),
                ('public_tests', models.TextField()),
                ('secret_tests', models.TextField()),
                ('sort_order', models.IntegerField(default=0)),
            ],
            options={
                'ordering': ['-sort_order'],
                'default_permissions': (),
            },
        ),
        migrations.CreateModel(
            name='ExamSubmitLog',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.TextField()),
                ('tests', models.TextField()),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('author', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to=settings.AUTH_USER_MODEL)),
                ('exam', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='exam.Exam')),
            ],
            options={
                'default_permissions': (),
            },
        ),
    ]
