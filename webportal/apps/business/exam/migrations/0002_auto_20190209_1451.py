# Generated by Django 2.1.4 on 2019-02-09 14:51

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('exam', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='exam',
            options={'default_permissions': ()},
        ),
        migrations.RemoveField(
            model_name='exam',
            name='sort_order',
        ),
    ]
