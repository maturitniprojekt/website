from django.urls import path

from . import views

app_name = 'exam'
urlpatterns = [
    path('create/', views.ExamCreateView.as_view(), name='create'),
    path('<int:pk>/update/', views.ExamUpdateView.as_view(), name='update'),
    path('<int:pk>/detail/', views.ExamDetailView.as_view(), name='detail'),
]
