import django_filters
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Field, Submit

from apps.business.course.models import Course


class CourseFilter(django_filters.FilterSet):
    title = django_filters.CharFilter(lookup_expr='icontains')
    # programming_language field let be created from model field automatically

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.form.helper = FormHelper()
        self.form.helper.layout = self.get_layout()
        self.form.helper.disable_csrf = True
        self.form.helper.form_method = 'get'

    @staticmethod
    def get_layout():
        return Layout(
            Field('title'),
            Field('programming_language'),

            Submit('filter', 'Filter')
        )

    class Meta:
        model = Course
        fields = ['title', 'programming_language']
