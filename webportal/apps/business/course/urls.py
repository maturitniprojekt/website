from django.urls import include, path

from apps.business.course import views

specific_urls = [
    path('update/', views.CourseUpdateView.as_view(), name='update'),
    path('detail/', views.CourseDetailView.as_view(), name='detail'),
]

app_name = 'course'
urlpatterns = [
    path('create/', views.CourseCreateView.as_view(), name='create'),
    path('list/', views.CourseListView.as_view(), name='list'),

    path('<int:pk>/', include(specific_urls)),
]
