from django.contrib.auth.decorators import permission_required
from django.contrib.contenttypes.models import ContentType
from django.db.models import Q
from django.utils.decorators import method_decorator
from django.views.generic import CreateView, UpdateView, ListView, DetailView
from guardian.mixins import PermissionRequiredMixin
from guardian.shortcuts import assign_perm

from apps.business.course import forms
from apps.business.course import filters
from apps.business.course.models import Course
from apps.business.exam.models import Exam
from apps.business.exercise.models import Exercise
from apps.core.account.mixins import AnyPermissionListMixin, AnyPermissionRequiredMixin


@method_decorator(permission_required('course.create_course'), name='dispatch')
class CourseCreateView(CreateView):
    model = Course
    form_class = forms.CourseCreateForm
    template_name = 'course/create.html'

    def form_valid(self, form):
        response = super().form_valid(form)
        assign_perm('course.manage_course', self.request.user, self.object)
        return response


class CourseUpdateView(PermissionRequiredMixin, UpdateView):
    permission_required = 'course.manage_course'

    model = Course
    form_class = forms.CourseUpdateForm
    template_name = 'course/update.html'

    def get_success_url(self):
        return self.request.GET.get('next', self.object.get_absolute_url())


class CourseListView(AnyPermissionListMixin, ListView):
    permission_required = ['course.manage_course', 'course.view_course']

    model = Course
    context_object_name = 'courses'
    template_name = 'course/list.html'

    def get_queryset(self, *args, **kwargs):
        qs = super().get_queryset()
        self.filter = filters.CourseFilter(self.request.GET, queryset=qs)
        return self.filter.qs

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data()

        # completed_section_items = {}
        #
        # for course in context.get('courses', context.get('object_list')):
        #     completed_section_items_count = course.section_set.filter(
        #         (Q(sectionitem__exercise__exercisesubmissionlog__tests_passed=True)
        #          & Q(sectionitem__exercise__exercisesubmissionlog__is_final=True)
        #          & Q(sectionitem__exercise__exercisesubmissionlog__author=self.request.user))
        #         |
        #         (Q(sectionitem__exam__examsubmissionlog__tests_passed=True)
        #          & Q(sectionitem__exam__examsubmissionlog__is_final=True)
        #          & Q(sectionitem__exam__examsubmissionlog__author=self.request.user))
        #     ).distinct().values_list('sectionitem', flat=True).count()
        #     completed_section_items.update({
        #         course.pk: completed_section_items_count
        #     })
        #
        # print(completed_section_items)

        context.update({
            'filter': self.filter,
            # 'completed_section_items': completed_section_items
        })
        return context


class CourseDetailView(AnyPermissionRequiredMixin, DetailView):
    permission_required = ['course.manage_course', 'course.view_course']

    model = Course
    context_object_name = 'course'
    template_name = 'course/detail.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        completed_sections = self.object.section_set.filter(
            (Q(sectionitem__exercise__exercisesubmissionlog__tests_passed=True)
             & Q(sectionitem__exercise__exercisesubmissionlog__is_final=True)
             & Q(sectionitem__exercise__exercisesubmissionlog__author=self.request.user))
            |
            (Q(sectionitem__exam__examsubmissionlog__tests_passed=True)
             & Q(sectionitem__exam__examsubmissionlog__is_final=True)
             & Q(sectionitem__exam__examsubmissionlog__author=self.request.user))
        ).distinct()
        context.update({
            'completed_section_item_pks': completed_sections.values_list('sectionitem', flat=True),
            'content_types': {
                'exercise': ContentType.objects.get_for_model(Exercise),
                # 'exam': ContentType.objects.get_for_model(Exam),
            }
        })
        return context
