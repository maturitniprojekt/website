from django.db import models
from django.urls import reverse

from apps.business.permission.mixins import ModelObjectLevelPermissionsMixin
from apps.business.shared.course_difficulty.models import CourseDifficulty
from apps.business.shared.programming_language.models import ProgrammingLanguage


class Course(models.Model, ModelObjectLevelPermissionsMixin):
    title = models.CharField(max_length=255)
    programming_language = models.ForeignKey(ProgrammingLanguage, on_delete=models.PROTECT)
    difficulty = models.ForeignKey(CourseDifficulty, on_delete=models.PROTECT)
    description = models.TextField(blank=True, null=True)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    sort_order = models.IntegerField(default=0)

    objects = models.Manager()

    def get_absolute_url(self):
        return reverse('course:detail', kwargs={'pk': self.pk})

    def get_update_url(self):
        return reverse('course:update', kwargs={'pk': self.pk})

    def __str__(self):
        return self.title

    class Meta:
        ordering = ['-sort_order', '-created_at']

        default_permissions = ()
        permissions = (
            ('create_course', 'Create course'),
            ('view_course', 'View course'),
            ('manage_course', 'Manage course')
        )
