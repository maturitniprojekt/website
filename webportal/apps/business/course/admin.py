from guardian.admin import GuardedModelAdmin

from webportal.admin import clientadmin
from .models import Course

clientadmin.register(Course, GuardedModelAdmin)
