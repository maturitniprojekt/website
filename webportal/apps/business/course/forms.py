from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Field, Submit
from django import forms

from apps.business.course.models import Course


class CourseCreateForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.helper = FormHelper()
        self.helper.layout = self.get_layout()

    @staticmethod
    def get_layout():
        return Layout(
            Field('title'),
            Field('programming_language'),
            Field('difficulty'),
            Field('description'),

            Submit('create', 'Create course')
        )

    class Meta:
        model = Course
        fields = ['title', 'programming_language', 'difficulty', 'description']


class CourseUpdateForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.helper = FormHelper()
        self.helper.layout = self.get_layout()

    @staticmethod
    def get_layout():
        return Layout(
            Field('title'),
            Field('programming_language'),
            Field('difficulty'),
            Field('description'),

            Submit('update', 'Update course')
        )

    class Meta:
        model = Course
        fields = ['title', 'programming_language', 'difficulty', 'description']
