from apps.business.course.models import Course
from apps.business.task import forms
from apps.business.task.models import Task
from apps.core.account.mixins import AnyPermissionRequiredMixin
from django.http import JsonResponse
from django.views.generic import CreateView, UpdateView, FormView
from guardian.mixins import PermissionRequiredMixin


class TaskCreateView(PermissionRequiredMixin, CreateView):
    permission_required = 'course.manage_course'

    course = None

    model = Task
    form_class = forms.TaskCreateForm
    template_name = 'task/create.html'

    def dispatch(self, request, *args, **kwargs):
        self.course = Course.objects.get(pk=kwargs.get('course_pk'))
        return super().dispatch(request, *args, **kwargs)

    def get_permission_object(self):
        return self.course

    def form_valid(self, form):
        task = form.save(commit=False)
        task.course = self.course
        task.save()
        return super().form_valid(form)


class TaskUpdateView(PermissionRequiredMixin, UpdateView):
    permission_required = 'course.manage_course'

    task = None

    model = Task
    form_class = forms.TaskUpdateForm
    template_name = 'task/update.html'

    def dispatch(self, request, *args, **kwargs):
        self.task = self.get_object()
        return super().dispatch(request, *args, **kwargs)

    def get_permission_object(self):
        task = self.get_object()
        return task.course

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs.update({
            'lang': self.task.language.compiler_name
        })
        return kwargs


class TaskDetailView(AnyPermissionRequiredMixin, FormView):
    permission_required = ['course.manage_course', 'course.view_course']

    task = None
    compile_response = None

    form_class = forms.TaskSubmissionForm
    template_name = 'task/detail.html'

    def dispatch(self, request, *args, **kwargs):
        self.task = Task.objects.get(pk=kwargs.get('pk'))
        return super().dispatch(request, *args, **kwargs)

    def get_permission_object(self):
        return self.task.course

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'task': self.task,
            'compile_response': self.compile_response
        })
        return context

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs.update({
            'initial': {
                'code': self.task.code_skeleton,
                'tests': self.task.public_tests
            },
            'lang': self.task.language.compiler_name
        })
        return kwargs

    def form_valid(self, form):
        return JsonResponse(form.compiler_response, safe=False)

    def form_invalid(self, form):
        return JsonResponse(form.compiler_response, safe=False)
