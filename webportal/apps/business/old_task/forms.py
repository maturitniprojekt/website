import requests
from apps.business.language.models import Language
from apps.business.task.models import Task
from apps.platform.data_select.widgets import DataSelect
from apps.platform.monaco_editor.widgets import MonacoEditor
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Field, Submit, Div
from django import forms


class TaskCreateForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.fields['code_skeleton'].widget = MonacoEditor(language=None)
        self.fields['public_tests'].widget = MonacoEditor(language=None)
        self.fields['secret_tests'].widget = MonacoEditor(language=None)

        self.helper = FormHelper()
        self.helper.layout = self.get_layout()

    @staticmethod
    def get_layout():
        return Layout(
            Div(
                Div(
                    Field('title'),
                    css_class='col-md-8'
                ),
                Div(
                    Field('language'),
                    css_class='col-md-4'
                ),
                css_class='row'
            ),
            Field('description'),
            Field('code_skeleton'),
            Field('public_tests'),
            Field('secret_tests'),

            Submit('create', 'Create task')
        )

    class Meta:
        model = Task
        fields = ['language', 'title', 'description', 'code_skeleton', 'public_tests', 'secret_tests']
        widgets = {
            'language': DataSelect(
                data_attrs=
                {
                    lang.pk: {
                        'data-editor-language': lang.editor_name
                    } for lang in Language.objects.all()
                }
            )
        }


class TaskUpdateForm(forms.ModelForm):
    def __init__(self, lang, *args, **kwargs):
        super().__init__(*args, **kwargs)

        print("lang", lang)
        self.lang = lang

        self.fields['code_skeleton'].widget = MonacoEditor(language=lang)
        self.fields['public_tests'].widget = MonacoEditor(language=lang)
        self.fields['secret_tests'].widget = MonacoEditor(language=lang)

        self.helper = FormHelper()
        self.helper.layout = self.get_layout()

    @staticmethod
    def get_layout():
        return Layout(
            Field('title'),
            Field('description'),
            Field('code_skeleton'),
            Field('public_tests'),
            Field('secret_tests'),

            Submit('update', 'Update task')
        )

    class Meta:
        model = Task
        fields = ['title', 'description', 'code_skeleton', 'public_tests', 'secret_tests']


class TaskSubmissionForm(forms.Form):
    lang = None
    compiler_response = None

    code = forms.CharField()
    tests = forms.CharField()

    def __init__(self, lang, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.lang = lang

        self.fields['code'].widget = MonacoEditor(language=lang)
        self.fields['tests'].widget = MonacoEditor(language=lang)

        self.helper = FormHelper()
        self.helper.layout = self.get_layout()

        self.helper.form_id = 'taskSubmitForm'
        # self.helper.form_method = 'post'
        # self.helper.form_action = 'hello'

    def is_valid(self):
        fields_valid = super().is_valid()

        if not fields_valid:
            return fields_valid

        data = {
            'lang': self.lang,
            'code': self.cleaned_data['code'],
            'tests': self.cleaned_data['tests'],
        }
        r = requests.post('http://host.docker.internal:8001/executor/execute-tests', data=data)
        r_json = r.json()

        print(r_json)

        self.compiler_response = r_json

        result = r_json['output']['result']['convertedResult']
        compile_valid = result and result.get('failures') == 0

        # self.add_error(NON_FIELD_ERRORS, 'Tests failed')

        return compile_valid

    @staticmethod
    def get_layout():
        return Layout(
            Field('code'),
            Field('tests'),

            Submit('submit', 'Submit')
        )
