from apps.business.course.models import Course
from apps.business.language.models import Language
from django.db import models
from django.urls import reverse


class Task(models.Model):
    course = models.ForeignKey(Course, on_delete=models.PROTECT)

    # TODO add composite unique index?? maybe not
    title = models.CharField(max_length=255)
    description = models.TextField()

    code_skeleton = models.TextField()
    public_tests = models.TextField()
    secret_tests = models.TextField()

    sort_order = models.IntegerField(default=0)

    objects = models.Manager()

    def get_absolute_url(self):
        return reverse('task:detail', kwargs={'pk': self.pk})

    def __str__(self):
        return self.title

    class Meta:
        ordering = ['-sort_order']

        default_permissions = ()
