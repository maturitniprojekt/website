from apps.business.task import views
from django.urls import include, path

specific_urls = [
    path('update/', views.TaskUpdateView.as_view(), name='update'),
    path('detail/', views.TaskDetailView.as_view(), name='detail'),
]

app_name = 'task'
urlpatterns = [
    path('create/course/<int:course_pk>/', views.TaskCreateView.as_view(), name='create'),

    path('<int:pk>/', include(specific_urls)),
]
