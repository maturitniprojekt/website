from django.views.generic import TemplateView

from apps.core.account.middleware import login_not_required


@login_not_required
class PublicHomeView(TemplateView):
    template_name = 'page/home_public.html'


class ClientHomeView(TemplateView):
    template_name = 'page/home_client.html'
