from django.urls import path, reverse_lazy
from django.views.generic.base import RedirectView

from apps.business.page.urls_client import login_not_required


@login_not_required
class TempRedirect(RedirectView):
    url = reverse_lazy('client:request')
    permanent = False


app_name = 'page'
urlpatterns = [
    path('', TempRedirect.as_view(), name='home'),
    # path('', PublicHomeView.as_view(), name='home')
]
