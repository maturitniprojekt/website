from django.urls import path, reverse_lazy
from django.views.generic.base import RedirectView

from apps.core.account.middleware import login_not_required


@login_not_required
class TempRedirect(RedirectView):
    url = reverse_lazy('course:list')
    permanent = False


app_name = 'page'
urlpatterns = [
    path('', TempRedirect.as_view(), name='home')
    # path('', ClientHomeView.as_view(), name='home')
]
