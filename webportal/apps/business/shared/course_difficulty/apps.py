from django.apps import AppConfig


class CourseDifficultyConfig(AppConfig):
    name = 'course_difficulty'
