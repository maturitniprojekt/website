from webportal.admin import publicadmin
from .models import CourseDifficulty

publicadmin.register(CourseDifficulty)
