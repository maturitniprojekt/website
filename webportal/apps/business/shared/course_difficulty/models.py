from django.db import models


class CourseDifficulty(models.Model):
    name = models.CharField(max_length=255, unique=True)

    sort_order = models.IntegerField(default=0)

    objects = models.Manager()

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['-sort_order']
