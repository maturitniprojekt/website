from webportal.admin import publicadmin
from .models import ProgrammingLanguage

publicadmin.register(ProgrammingLanguage)
