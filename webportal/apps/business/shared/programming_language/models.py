from django.db import models


class ProgrammingLanguage(models.Model):
    human_name = models.CharField(max_length=255)

    editor_name = models.CharField(max_length=255)
    code_runner_name = models.CharField(max_length=255)

    sort_order = models.IntegerField(default=0)

    objects = models.Manager()

    class Meta:
        ordering = ['-sort_order']

    def __str__(self):
        return self.human_name
