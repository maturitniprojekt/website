from django.apps import AppConfig


class MultiselectConfig(AppConfig):
    name = 'multiselect'
