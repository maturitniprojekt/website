from django.forms import SelectMultiple


class MultiSelectWidget(SelectMultiple):
    def __init__(self):
        attrs = {
            'class': 'multiselect'
        }
        super().__init__(attrs=attrs)
