from django.forms import Textarea


# FIXME not showing errors
class MonacoEditor(Textarea):
    template_name = 'monaco_editor/monaco_editor.html'

    def __init__(self, language):
        attrs = {
            'data-language': language,
            'hidden': True,
        }
        super().__init__(attrs=attrs)
