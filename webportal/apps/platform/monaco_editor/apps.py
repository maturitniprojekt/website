from django.apps import AppConfig


class MonacoEditorConfig(AppConfig):
    name = 'monaco_editor'
