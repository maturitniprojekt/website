from django.apps import AppConfig


class DataSelectConfig(AppConfig):
    name = 'data_select'
