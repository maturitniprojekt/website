from django.forms import Select


class DataSelect(Select):
    data_attrs = {}

    def __init__(self, data_attrs, **kwargs):
        super().__init__(**kwargs)

        self.data_attrs = data_attrs

    def create_option(self, name, value, label, selected, index, subindex=None, attrs=None):
        option_dict = super().create_option(name, value, label, selected, index, subindex, attrs)

        if option_dict.get('value') in self.data_attrs:
            option_dict['attrs'].update(self.data_attrs[option_dict['value']])

        return option_dict
