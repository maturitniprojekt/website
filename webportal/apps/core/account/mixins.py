from django.conf import settings
from django.contrib.auth import REDIRECT_FIELD_NAME
from django.core.exceptions import PermissionDenied, ObjectDoesNotExist
from django.http import HttpResponseForbidden, HttpResponseNotFound
from django.shortcuts import render_to_response
from django.template import RequestContext
from guardian.conf import settings as guardian_settings
from guardian.mixins import PermissionRequiredMixin, PermissionListMixin


# Original function get_40x_or_None taken from guardian.utils
# Changed to act as find only one valid permission to get condition to True
def get_any_40x_or_None(request, perms, obj=None, login_url=None,
                        redirect_field_name=None, return_403=False,
                        return_404=False, accept_global_perms=False):
    login_url = login_url or settings.LOGIN_URL
    redirect_field_name = redirect_field_name or REDIRECT_FIELD_NAME

    # Handles both original and with object provided permission check
    # as ``obj`` defaults to None

    has_permissions = False
    # global perms check first (if accept_global_perms)
    if accept_global_perms:
        has_permissions = any(request.user.has_perm(perm) for perm in perms)  # Here was change from all -> any
    # if still no permission granted, try obj perms
    if not has_permissions:
        has_permissions = any(request.user.has_perm(perm, obj)
                              for perm in perms)  # Here was change from all -> any

    if not has_permissions:
        if return_403:
            if guardian_settings.RENDER_403:
                response = render_to_response(
                    guardian_settings.TEMPLATE_403, {},
                    RequestContext(request))
                response.status_code = 403
                return response
            elif guardian_settings.RAISE_403:
                raise PermissionDenied
            return HttpResponseForbidden()
        if return_404:
            if guardian_settings.RENDER_404:
                response = render_to_response(
                    guardian_settings.TEMPLATE_404, {},
                    RequestContext(request))
                response.status_code = 404
                return response
            elif guardian_settings.RAISE_404:
                raise ObjectDoesNotExist
            return HttpResponseNotFound()
        else:
            from django.contrib.auth.views import redirect_to_login
            return redirect_to_login(request.get_full_path(),
                                     login_url,
                                     redirect_field_name)


class AnyPermissionRequiredMixin(PermissionRequiredMixin):
    def check_permissions(self, request):
        """
        Checks if *request.user* has all permissions returned by
        *get_required_permissions* method.

        :param request: Original request.
        """
        obj = self.get_permission_object()

        # Here was change to local ANY function - from get_40x_or_None to get_any_40x_or_None
        forbidden = get_any_40x_or_None(request,
                                        perms=self.get_required_permissions(request),
                                        obj=obj,
                                        login_url=self.login_url,
                                        redirect_field_name=self.redirect_field_name,
                                        return_403=self.return_403,
                                        return_404=self.return_404,
                                        accept_global_perms=self.accept_global_perms
                                        )

        if forbidden:
            self.on_permission_check_fail(request, forbidden, obj=obj)
        if forbidden and self.raise_exception:
            raise PermissionDenied()
        return forbidden


# Extended PermissionListMixin that accepts any permission that evaluates True
class AnyPermissionListMixin(PermissionListMixin):
    get_objects_for_user_extra_kwargs = {
        'any_perm': True
    }
