from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.urls import reverse


def get_view(view_path):
    i = view_path.rfind('.')
    module_path, view_name = view_path[:i], view_path[i + 1:]
    module = __import__(module_path, globals(), locals(), [view_name])
    return getattr(module, view_name)


def matches_public_view(public_views, view):
    if public_views:
        if hasattr(view, 'view_class'):
            if view.view_class in public_views:
                return True

        if view in public_views:
            return True


def check_decorator(view):
    if hasattr(view, 'view_class'):
        if hasattr(view.view_class, 'LOGIN_NOT_REQUIRED'):
            return view.view_class.LOGIN_NOT_REQUIRED

    if hasattr(view, 'LOGIN_NOT_REQUIRED'):
        return view.LOGIN_NOT_REQUIRED

    return False


class GlobalLoginRequiredMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

        self.public_views = []

        if hasattr(settings, 'PUBLIC_VIEWS'):
            for view_path in settings.PUBLIC_VIEWS:
                view = get_view(view_path)
                self.public_views.append(view)

    def __call__(self, request):
        return self.get_response(request)

    def process_view(self, request, view_func, view_args, view_kwargs):
        if request.user.is_authenticated:
            return None

        if request.path.startswith(reverse('admin:index')):
            return None

        if matches_public_view(self.public_views, view_func) or check_decorator(view_func):
            return None

        return login_required(view_func)(request, *view_args, **view_kwargs)


def login_not_required(the_view):
    def getter(self):
        return getattr(self, '_LOGIN_NOT_REQUIRED', True)

    setattr(the_view, 'LOGIN_NOT_REQUIRED', property(getter))

    return the_view
