from django.contrib.auth.views import (
    LogoutView as BaseLogoutView,
    LoginView as BaseLoginView
)
from django.urls import reverse_lazy
from django.views.generic import TemplateView

from apps.core.account.forms import LoginForm
from apps.core.account.middleware import login_not_required


@login_not_required
class LoginView(BaseLoginView):
    form_class = LoginForm
    success_url = reverse_lazy('page:home')
    template_name = 'account/login.html'


@login_not_required
class RegisterView(TemplateView):
    template_name = 'account/register.html'


@login_not_required
class LogoutView(BaseLogoutView):
    template_name = 'account/logout.html'
