from django.contrib.auth.admin import UserAdmin, GroupAdmin
from django.contrib.auth.models import Group

from apps.core.account.models import User
from webportal.admin import publicadmin, clientadmin

publicadmin.register(User, UserAdmin)
publicadmin.register(Group, GroupAdmin)

clientadmin.register(User, UserAdmin)
clientadmin.register(Group, GroupAdmin)
