from webportal.admin import publicadmin
from .models import ClientRequest, Client

publicadmin.register(Client)
publicadmin.register(ClientRequest)
