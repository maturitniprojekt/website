from django.core.management.base import BaseCommand
from tenant_schemas.utils import tenant_context

from apps.business.shared.course_difficulty.models import CourseDifficulty
from apps.business.shared.programming_language.models import ProgrammingLanguage
from apps.core.account.models import User
from apps.core.client.models import Client


class Command(BaseCommand):
    help = 'Creating basic public a private client'
    requires_migrations_checks = True

    def handle(self, *args, **kwargs):
        public_tenant = Client.objects.create(
            domain_url='a.matproj.local',
            schema_name='public',
            full_name='Public tenant',
            short_name='Public'
        )

        with tenant_context(public_tenant):
            ProgrammingLanguage.objects.create(human_name='C#', editor_name='csharp', code_runner_name='csharp',
                                               sort_order=1)
            ProgrammingLanguage.objects.create(human_name='PHP', editor_name='php', code_runner_name='php',
                                               sort_order=0)

            CourseDifficulty.objects.create(name='Jednoduche', sort_order=2)
            CourseDifficulty.objects.create(name='Stredni', sort_order=1)
            CourseDifficulty.objects.create(name='Tezke', sort_order=0)

            User.objects.create_superuser(username='admin', password='admin', email='')

        private_tenant = Client.objects.create(
            domain_url='b.matproj.local',
            schema_name='tenant_b',
            full_name='Tenant B',
            short_name='ten b'
        )

        with tenant_context(private_tenant):
            User.objects.create_superuser(username='admin', password='admin', email='')

        self.stdout.write("Initialized successfully.")
