from django.core.management.base import BaseCommand

from apps.core.client.models import Client


class Command(BaseCommand):
    help = 'Created basic public client'
    requires_migrations_checks = True

    def add_arguments(self, parser):
        parser.add_argument('domain_url', type=str, help='Client domain url')
        parser.add_argument('schema_name', type=str, help='Client schema name')
        parser.add_argument('full_name', type=str, help='Client full name')
        parser.add_argument('short_name', type=str, help='Client short name')

    def handle(self, *args, **kwargs):
        domain_url = kwargs['domain_url']
        schema_name = kwargs['schema_name']
        full_name = kwargs['full_name']
        short_name = kwargs['short_name']

        Client.objects.create(
            domain_url=domain_url,
            schema_name=schema_name,
            full_name=full_name,
            short_name=short_name
        )

        self.stdout.write("Client created successfully.")
