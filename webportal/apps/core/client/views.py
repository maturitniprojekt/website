from django.urls import reverse_lazy
from django.views.generic import CreateView, TemplateView

from apps.core.account.middleware import login_not_required
from .forms import RequestForm


@login_not_required
class RequestView(CreateView):
    template_name = 'client/request.html'
    form_class = RequestForm
    success_url = reverse_lazy('client:request_done')


@login_not_required
class RequestDoneView(TemplateView):
    template_name = 'client/request_done.html'
