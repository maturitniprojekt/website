from django.db import models

from tenant_schemas.models import TenantMixin


class ClientRequest(models.Model):
    full_name = models.CharField(max_length=255)
    short_name = models.CharField(max_length=255)
    preferred_url = models.CharField(max_length=255)

    def __str__(self):
        return self.full_name


class Client(TenantMixin):
    full_name = models.CharField(max_length=255)
    short_name = models.CharField(max_length=255)

    # default true, schema will be automatically created and synced when it is saved
    auto_create_schema = True

    def __str__(self):
        return self.full_name
