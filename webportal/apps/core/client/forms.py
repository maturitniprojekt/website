from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Field, Submit
from django import forms

from .models import ClientRequest


class RequestForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.helper = FormHelper()
        self.helper.layout = self.get_layout()

    @staticmethod
    def get_layout():
        return Layout(
            Field('full_name'),
            Field('short_name'),
            Field('preferred_url'),

            Submit('create', 'Create request')
        )

    class Meta:
        model = ClientRequest
        fields = ['full_name', 'short_name', 'preferred_url']
