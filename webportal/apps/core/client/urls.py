from django.urls import path

from .views import RequestView, RequestDoneView

app_name = 'client'
urlpatterns = [
    path('request/', RequestView.as_view(), name='request'),
    path('request/done/', RequestDoneView.as_view(), name='request_done')
]
