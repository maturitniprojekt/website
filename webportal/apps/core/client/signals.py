from django.db.models.signals import post_save
from django.dispatch import receiver
from tenant_schemas.utils import tenant_context

from apps.core.account.models import User
from .models import Client

# TODO make working
# @receiver(post_save, sender=Client)
# def create_tenant_superuser(sender, instance, created, **kwargs):
#     print(instance)
#
#     if created:
#         print(instance)
#         with tenant_context(instance):
#             User.objects.create_superuser('admin123', 'luboshbhb@atlas.cz', 'admin123')
