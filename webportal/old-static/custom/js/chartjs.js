Chart.defaults.global.tooltips.enabled = false;
Chart.defaults.global.tooltips.mode = 'index';
Chart.defaults.global.tooltips.mode = 'nearest';

Chart.defaults.global.animation.duration = 0;

Chart.defaults.global.legend.display = false;
