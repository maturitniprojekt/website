from django.contrib.admin import AdminSite


class PublicAdminSite(AdminSite):
    site_header = 'Public admin'


publicadmin = PublicAdminSite(name='publicadmin')


class ClientAdminSite(AdminSite):
    site_header = 'Client admin'


clientadmin = ClientAdminSite(name='clientadmin')
