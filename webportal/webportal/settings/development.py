from .base import *

# ==============================================================================
# Generic Django project settings
# ==============================================================================

DEBUG = True

# ==============================================================================
# Django debug toolbar
# ==============================================================================

INSTALLED_APPS += [
    'debug_toolbar'
]

MIDDLEWARE = [
    'debug_toolbar.middleware.DebugToolbarMiddleware'
] + MIDDLEWARE


DEBUG_TOOLBAR_CONFIG = {
    'SHOW_TOOLBAR_CALLBACK': lambda request: True
}

# ==============================================================================
# Templates
# ==============================================================================

WEBPACK_LOADER['DEFAULT']['CACHE'] = False

# ==============================================================================
# Mock up email
# ==============================================================================

EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'
