import os

BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

# ==============================================================================
# Generic Django project settings
# ==============================================================================

DEBUG = False

SECRET_KEY = os.environ.get('SECRET_KEY')

ALLOWED_HOSTS = ['*']

STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(BASE_DIR, 'static')
STATICFILES_DIRS = [
    os.path.join(BASE_DIR, 'assets', 'dist')
]

SHARED_APPS = [
    # Precedence apps
    'tenant_schemas',
    'apps.core.account',

    # Contrib apps
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.sessions',
    'django.contrib.contenttypes',
    'guardian',

    # Shared apps
    'apps.core.client',
    'apps.business.shared.programming_language',
    'apps.business.shared.course_difficulty',

]

TENANT_APPS = [
    # Precedence apps
    'apps.core.account',

    # 3rd party apps
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.sessions',
    'django.contrib.contenttypes',
    'guardian',

    # Custom apps
    'apps.business.course',
    'apps.business.section',
    'apps.business.exercise',
    'apps.business.exam'
]

INSTALLED_APPS = [
    # Precedence apps
    'tenant_schemas',

    # 3rd party apps
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'webpack_loader',
    'crispy_forms',
    'guardian',
    'active_link',
    'django_filters',

    # Custom universal apps
    'apps.platform.monaco_editor',

    # Core apps
    'apps.core.account',
    'apps.core.client',

    # Shared apps
    'apps.business.shared.programming_language',
    'apps.business.shared.course_difficulty',

    # Custom apps
    'apps.business.course',
    'apps.business.section',
    'apps.business.exercise',
    'apps.business.exam',

    'apps.business.permission',

    # Pages
    'apps.business.page'
]

# ==============================================================================
# App settings
# ==============================================================================

AUTH_USER_MODEL = 'account.User'

LOGIN_REDIRECT_URL = 'page:home'
LOGIN_URL = 'account:login'

# ==============================================================================
# App authentication
# ==============================================================================

AUTHENTICATION_BACKENDS = [
    'django.contrib.auth.backends.ModelBackend',
    'guardian.backends.ObjectPermissionBackend',
]
GUARDIAN_MONKEY_PATCH = False
ANONYMOUS_USER_NAME = None

# ==============================================================================
# Multi tenant settings
# ==============================================================================

TENANT_MODEL = 'client.Client'

DEFAULT_FILE_STORAGE = 'tenant_schemas.storage.TenantFileSystemStorage'

# ==============================================================================
# Routing
# ==============================================================================

ROOT_URLCONF = 'webportal.urls_client'
PUBLIC_SCHEMA_URLCONF = 'webportal.urls_public'

# ==============================================================================
# Templates
# ==============================================================================

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            os.path.join(BASE_DIR, 'templates'),
            # os.path.join(BASE_DIR, 'vendor_templates'),
        ]
        ,
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WEBPACK_LOADER = {
    'DEFAULT': {
        'CACHE': True,
        'BUNDLE_DIR_NAME': '/',  # must end with slash
        'STATS_FILE': os.path.join(BASE_DIR, 'assets', 'dist', 'webpack-stats.json'),
        'POLL_INTERVAL': 0.1,
        'TIMEOUT': None,
        'IGNORE': [r'.+\.hot-update.js', r'.+\.map']
    }
}

# ==============================================================================
# Middleware
# ==============================================================================

MIDDLEWARE = [
    'tenant_schemas.middleware.TenantMiddleware',

    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',

    'apps.core.account.middleware.GlobalLoginRequiredMiddleware',
]

WSGI_APPLICATION = 'webportal.wsgi.application'

# ==============================================================================
# Database settings
# ==============================================================================

DATABASES = {
    'default': {
        'ENGINE': 'tenant_schemas.postgresql_backend',
        'NAME': os.environ.get('DB_NAME'),
        'USER': os.environ.get('DB_USER'),
        'PASSWORD': os.environ.get('DB_PASSWORD'),
        'HOST': os.environ.get('DB_HOST'),
        'PORT': os.environ.get('DB_PORT'),
    }
}

DATABASE_ROUTERS = (
    'tenant_schemas.routers.TenantSyncRouter',
)

# ==============================================================================
# Bootstrap 4 form renderer settings
# ==============================================================================
CRISPY_TEMPLATE_PACK = 'bootstrap4'

# ==============================================================================
# Localization
# ==============================================================================
TIME_ZONE = 'Europe/Prague'
