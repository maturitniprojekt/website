from django.conf import settings
from django.urls import path, include

from webportal.admin import clientadmin

urlpatterns = [
    path('superadministration/', clientadmin.urls),

    path('account/', include('apps.core.account.urls')),
    
    path('course/', include('apps.business.course.urls')),
    path('section/', include('apps.business.section.urls')),
    path('exercise/', include('apps.business.exercise.urls')),
    path('exam/', include('apps.business.exam.urls')),

    path('permission/', include('apps.business.permission.urls')),

    path('', include('apps.business.page.urls_client')),
]

if settings.DEBUG:
    import debug_toolbar

    urlpatterns = [path('__debug__/', include(debug_toolbar.urls))] + urlpatterns
