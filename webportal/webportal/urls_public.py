from django.conf import settings
from django.urls import path, include

from webportal.admin import publicadmin

urlpatterns = [
    path('superadministration/', publicadmin.urls),
    path('client/', include('apps.core.client.urls')),
    path('', include('apps.business.page.urls_public')),
]

if settings.DEBUG:
    import debug_toolbar

    urlpatterns = [path('__debug__/', include(debug_toolbar.urls))] + urlpatterns
